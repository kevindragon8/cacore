package me.kevindragon.cacore;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import com.connorlinfoot.bountifulapi.BountifulAPI;

public class command implements CommandExecutor {

	@SuppressWarnings("deprecation")
	public boolean onCommand(final CommandSender sender, Command command, String commandLbl, String[] args) {

		if (command.getName().equalsIgnoreCase("build")) {

			Player p = (Player) sender;

			double x = 0.5;
			double y = 65;
			double z = 0.5;

			p.teleport((new Location(Bukkit.getWorld("plotworld"), x, y, z)));
			p.setGameMode(GameMode.CREATIVE);

			p.sendMessage("");
			p.sendMessage("              " + ChatColor.GRAY + "" + ChatColor.BOLD + "" + ChatColor.STRIKETHROUGH + "--["
					+ ChatColor.AQUA + "" + ChatColor.BOLD + "C.A." + ChatColor.GREEN + "" + ChatColor.BOLD + "BUILD"
					+ ChatColor.GRAY + "" + ChatColor.BOLD + "" + ChatColor.STRIKETHROUGH + "]--");
			p.sendMessage(ChatColor.AQUA + "" + ChatColor.BOLD + "           Welcome! :)");
			p.sendMessage(ChatColor.RED + "     Be sure to use the " + ChatColor.LIGHT_PURPLE + "WorldEdit Tool "
					+ ChatColor.RED + "! Type /WorldEditTool to obtain it!");
			p.sendMessage("");

			BountifulAPI.sendFullTitle(p, 15, 40, 20,
					ChatColor.AQUA + "" + ChatColor.ITALIC + "Welcome to " + ChatColor.AQUA + "" + ChatColor.BOLD
							+ "CA " + ChatColor.GREEN + "" + ChatColor.BOLD + "BUILD!",
					ChatColor.RED + "Type /WorldEditTool to obtain an Axe!");

		}

		if (command.getName().equalsIgnoreCase("spawn")) {
			Player p = (Player) sender;
			p.setGameMode(GameMode.SURVIVAL);
			


			double x = -268.5;
			double y = 71;
			double z = 284.5;

			p.teleport(new Location(Bukkit.getWorld("world"), x, y, z));

			
			/*
			 * if ((sender instanceof Player)) { return false;
			 * 
			 * } p.sendMessage(ChatColor.GRAY + "[" + ChatColor.AQUA +
			 * "Teleport" + ChatColor.GRAY + "] " + ChatColor.GOLD +
			 * "You will be teleported in 5 seconds!");
			 * 
			 * new BukkitRunnable() { public void run() {
			 * 
			 * 
			 * 
			 * 
			 * } }.runTaskLater(new Main(), 100);
			 */

		}

		if (command.getName().equalsIgnoreCase("coreinfo")) {

			Player p = (Player) sender;

			p.sendMessage("");
			p.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "Community Aid");
			p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Core Plugin");
			p.sendMessage("");
			p.sendMessage(ChatColor.AQUA + "" + ChatColor.ITALIC
					+ "Features: GoldCurrency, BuildFunctions, Spawn, Legendaries, FFA, Elytra Wars");
			p.sendMessage(ChatColor.LIGHT_PURPLE + "Coded by xKendra_");
			p.sendMessage(ChatColor.GREEN + "" + ChatColor.ITALIC + "Plugin Version " + Bukkit.getPluginManager().getPlugin("CommunityAidCore").getDescription());
			p.sendMessage("");

			p.playSound(p.getLocation(), Sound.BLOCK_ANVIL_HIT, 0, 2);

		}
		if (command.getName().equalsIgnoreCase("update")) {
			Player p = (Player) sender;
			Bukkit.broadcastMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + "Update" + ChatColor.GRAY + "] Server will be updating soon!");
		}
		if (command.getName().equalsIgnoreCase("commands")) {
			Player p = (Player) sender;
			p.sendMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + "Info" + ChatColor.GRAY + "] Availiable Commands: /docs /staff /events /ts /apply");
		}
		if (command.getName().equalsIgnoreCase("event")) {
			Player p = (Player) sender;
			p.sendMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + "Event" + ChatColor.GRAY + "] Upcoming event is " + ChatColor.BLUE + "FFA Tourney" + ChatColor.GRAY + "! The Event is on April 22nd ~ 7:30EST");
			p.sendMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + "Link" + ChatColor.GRAY + "] https://docs.google.com/document/d/14p30XSv8rnXJD8P7EX3z1dpKqFqTcceVaVb_tZuQPJ4/edit");
		}
		

		if (command.getName().equalsIgnoreCase("worldedittool")) {

			Player p = (Player) sender;

			ItemStack ironhelmet = new ItemStack(Material.WOOD_AXE);
			ItemMeta ironhelmetMeta = ironhelmet.getItemMeta();
			ironhelmetMeta.setDisplayName(ChatColor.LIGHT_PURPLE + "WorldEdit Tool");
			ArrayList<String> lore1 = new ArrayList();
			lore1.add(ChatColor.RED + "The Classic Tool.");
			ironhelmetMeta.setLore(lore1);
			ironhelmet.setItemMeta(ironhelmetMeta);
			ironhelmet.addUnsafeEnchantment(Enchantment.DURABILITY, 1000);

			p.getInventory().addItem(ironhelmet);

		}

		if (command.getName().equalsIgnoreCase("news")) {
			Player p = (Player) sender;

			if (args[0].equalsIgnoreCase("1")) {
				p.sendMessage("");

				p.sendMessage(ChatColor.AQUA + "" + ChatColor.BOLD + "" + ChatColor.ITALIC + "News");
				p.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + "<><><><><><><><><><>");
				p.sendMessage(ChatColor.WHITE + "- Some Gold Exploits Fixed");
				p.sendMessage("-Legendaries Prices Lowered");
				p.sendMessage("-Added some potions and Misc Items");
				p.sendMessage("Notice: Guns are Unlimited Ammo.");
				p.sendMessage(
						"-You can earn Custom Enchantments found on Legendaries and many more via Enchanting Table.");
				p.sendMessage("TIP: You can purchase EXP Bottles at the Misc Merchant.");
				p.sendMessage("");
				p.sendMessage(ChatColor.BOLD + "Written on 1/7/16 by KevinDragon (xAbigail_) Developer");
				p.sendMessage("");

			}

		}

		if (command.getName().equalsIgnoreCase("poke")) {

			Player target = Bukkit.getPlayerExact(args[0]);
			Player p = (Player) sender;
			if (args.length == 0) {
				p.sendMessage(
						ChatColor.GRAY + "[" + ChatColor.BLUE + "Poke" + ChatColor.GRAY + "] Choose someone to Poke!");
				p.playSound(p.getLocation(), Sound.ENTITY_VILLAGER_NO, 0, 2);
			}

			else {
				target.sendMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + "Poke" + ChatColor.GRAY
						+ "] You have been Poked by " + ChatColor.BLUE + p.getDisplayName() + ChatColor.GRAY + "!!!");
				target.playSound(target.getLocation(), Sound.ENTITY_GHAST_SCREAM, 2, 2);
				p.sendMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + "Poke" + ChatColor.GRAY + "] You poked "
						+ ChatColor.BLUE + target.getDisplayName() + ChatColor.GRAY + "!!!");
				target.playSound(target.getLocation(), Sound.ENTITY_GHAST_SCREAM, 2, 2);
				p.playSound(p.getLocation(), Sound.BLOCK_LEVER_CLICK, 0, 2);
			}
		}

		if (command.getName().equalsIgnoreCase("sannounce")) {
			Player p = (Player) sender;
			if (args.length == 0) {
				p.sendMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + "Announcer" + ChatColor.GRAY
						+ "] Type something to announce!");
				p.playSound(p.getLocation(), Sound.ENTITY_VILLAGER_NO, 0, 2);
			} else {
				Bukkit.broadcastMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + "Announcement" + ChatColor.GRAY + "] "
						+ ChatColor.BLUE + args.toString());
			}
		}
		
		if (command.getName().equalsIgnoreCase("apply")) {
			Player p = (Player) sender;
			p.sendMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + "Info" + ChatColor.GRAY + "] Currently, xKendra_ is accepting members who are loyal, dedicated, and show knowledge as potential candidates for " + ChatColor.GOLD + "" + ChatColor.BOLD +"MOD");
			p.sendMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + "Link" + ChatColor.GRAY + "] http://goo.gl/forms/ZuC7W7Ep1o");
		}
		
		
		
		if (command.getName().equalsIgnoreCase("ts")) {
			Player p = (Player) sender;

			p.sendMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + "Meeting" + ChatColor.GRAY + "] " + ChatColor.GRAY
					+ "There are no Meetings coming up. Maybe later? :)");
		}

		if (command.getName().equalsIgnoreCase("donate")) {
			Player p = (Player) sender;
			p.sendMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + "Info" + ChatColor.GRAY + "] " + ChatColor.GRAY
					+ "To donate, you send the money directly through paypal to " + ChatColor.BLUE
					+ "kevindragon123@gmail.com");
			p.sendMessage(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "  VIP - $5 - 3 Months Duration");
			p.sendMessage(ChatColor.GRAY + "    -/kit Vip");
			p.sendMessage(ChatColor.GRAY + "    -/wand Apprentice");
			p.sendMessage(ChatColor.GRAY + "    -/ce ench LifeSteal");
			p.sendMessage(ChatColor.GRAY + "    -/simpleparticles");
			p.sendMessage(ChatColor.GRAY + "      -All Tornado Particles");
			p.sendMessage(ChatColor.GRAY + "      -All Random Particles");
			p.sendMessage("");
			p.sendMessage(ChatColor.AQUA + "" + ChatColor.BOLD + "  VIP+ - $10 - 3 Months Duration");
			p.sendMessage(
					ChatColor.RED + "    Everything in Rank " + ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "VIP");
			p.sendMessage(ChatColor.GRAY + "    -/kit Vip+");
			p.sendMessage(ChatColor.GRAY + "    -/ce ench AutoRepair");
			p.sendMessage(ChatColor.GRAY + "    -/ce ench RocketBoots");
			p.sendMessage(ChatColor.GRAY + "    -/simpleparticles");
			p.sendMessage(ChatColor.GRAY + "      -All Circle Particles");
			p.sendMessage(ChatColor.GRAY + "      -All Ring Particles");
			p.sendMessage("");
		}

		if (command.getName().equalsIgnoreCase("warn")) {
			Player target = Bukkit.getPlayerExact(args[0]);
			Player p = (Player) sender;
			if (args.length == 1) {
				Bukkit.broadcastMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + "Warn" + ChatColor.GRAY + "] "
						+ ChatColor.YELLOW + target.getDisplayName() + ChatColor.GRAY + " was warned by " + ChatColor.RED
						+ p.getDisplayName());
			} else {
				p.sendMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + "Warn" + ChatColor.GRAY
						+ "] Please Specify a Player and a Reason!");
			}
		}

		if (command.getName().equalsIgnoreCase("docs")) {
			Player p = (Player) sender;

			p.sendMessage("");
			p.sendMessage(ChatColor.AQUA + "         " + ChatColor.GRAY + "[" + ChatColor.BLUE + "Documents"
					+ ChatColor.GRAY + "]");
			p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Main - https://goo.gl/JkVm5v");
			p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Server Activity Log - https://goo.gl/Li4IeV");
			p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Staff Terms - https://goo.gl/vwxrBT");
			p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Community FAQ - https://goo.gl/baKI3t");
			p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Punishment Guidelines - https://goo.gl/awp7V9");
			p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Staff Application - http://goo.gl/forms/A7NoFdEVJJ");
			p.sendMessage("");
		}

		if (command.getName().equalsIgnoreCase("staff")) {
			Player p = (Player) sender;

			p.sendMessage("");
			p.sendMessage(ChatColor.GRAY + "" + ChatColor.STRIKETHROUGH + "         -- " + ChatColor.BLUE + ""
					+ ChatColor.BOLD + "Staff List " + ChatColor.GRAY + "" + ChatColor.STRIKETHROUGH + "--");
			p.sendMessage("");
			p.sendMessage(ChatColor.BLUE + "" + ChatColor.BOLD + "OWNER");
			p.sendMessage(ChatColor.BLUE + "   xKendra_");
			p.sendMessage(ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "DEVELOPER");
			p.sendMessage(ChatColor.DARK_AQUA + "   xaanit");
			p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "ADMINISTRATOR");
			p.sendMessage(ChatColor.RED + "   Andrewlander_");
			p.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "SENIOR MODERATOR");
			p.sendMessage(ChatColor.GOLD + "   coopjc");
			p.sendMessage(ChatColor.GOLD + "   WowBen");
			p.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "MODERATORS");

		}

		if (command.getName().equalsIgnoreCase("spotion")) {
			Player p = (Player) sender;
			if (p.hasPermission("kevin.items")) {
				
				if (args[0].equalsIgnoreCase("1")) {

					ItemStack ironhelmet = new ItemStack(Material.POTION);
					ItemMeta ironhelmetMeta = ironhelmet.getItemMeta();
					ironhelmetMeta.setDisplayName(ChatColor.BLUE + "" + ChatColor.ITALIC + "Ambiguous " + ChatColor.WHITE + "Potion " + ChatColor.AQUA + "" + ChatColor.BOLD + "I");
					ArrayList<String> lore0 = new ArrayList();
					lore0.add(ChatColor.RED + "" + ChatColor.ITALIC + "These mysterious potions are");
					lore0.add(ChatColor.RED + "" + ChatColor.ITALIC + "not your average potion...");
					ironhelmetMeta.setLore(lore0);
					ironhelmet.setItemMeta(ironhelmetMeta);
					ironhelmet.addUnsafeEnchantment(Enchantment.THORNS, 10);
					p.getInventory().addItem(ironhelmet);
				}
			}
		}
		if (command.getName().equalsIgnoreCase("switchblade")) {
			Player p = (Player) sender;
			if (p.hasPermission("kevin.items")) {
			ItemStack ironhelmet = new ItemStack(Material.DIAMOND_SWORD);
			ItemMeta ironhelmetMeta = ironhelmet.getItemMeta();
			ironhelmetMeta.setDisplayName(ChatColor.BLUE + "" + ChatColor.BOLD + "SWITCH " + ChatColor.WHITE + "" + ChatColor.BOLD + "Blade");
			ArrayList<String> lore1 = new ArrayList();
			lore1.add(ChatColor.BLUE + "" + ChatColor.ITALIC + "Legendary" + ChatColor.GRAY + "-" +ChatColor.RED + "Be powerful, but weakened at once.");
			ironhelmetMeta.setLore(lore1);
			ironhelmet.setItemMeta(ironhelmetMeta);
			ironhelmet.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 7);

			p.getInventory().addItem(ironhelmet);
			
			}
		}

		if (command.getName().equalsIgnoreCase("goldensword")) {
			
			Player p = (Player) sender;
			if (p.hasPermission("kevin.items")) {
			
			ItemStack ironhelmet2 = new ItemStack(Material.GOLD_SWORD);
			ItemMeta ironhelmet2Meta = ironhelmet2.getItemMeta();
			ironhelmet2Meta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "GOLDEN " + ChatColor.YELLOW + "" + ChatColor.BOLD + "BLADE");
			ArrayList<String> lore2 = new ArrayList();
			lore2.add(ChatColor.AQUA + "" + ChatColor.ITALIC + "Legendary" + ChatColor.GRAY + "-" + ChatColor.RED + "May this weapon use the power of GOLD!");
			ironhelmet2Meta.setLore(lore2);
			ironhelmet2.setItemMeta(ironhelmet2Meta);

			p.getInventory().addItem(ironhelmet2);
			}
		}
		if (command.getName().equalsIgnoreCase("ffa2")) {
			Player p = (Player) sender;
			double x = -2.5;
			double y = 131;
			double z = 168.5;

			p.teleport((new Location(Bukkit.getWorld("spawn"), x, y, z)));
			p.setGameMode(GameMode.ADVENTURE);

			p.sendMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + "FFA" + ChatColor.GRAY
					+ "] Welcome to the Free For All Arena!");

			p.getInventory().setHelmet(new ItemStack(Material.IRON_HELMET));
			p.getInventory().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
			p.getInventory().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
			p.getInventory().setBoots(new ItemStack(Material.IRON_BOOTS));
			
			ItemStack ironhelmet2 = new ItemStack(Material.DIAMOND_SWORD);
			ItemMeta ironhelmet2Meta = ironhelmet2.getItemMeta();
			ironhelmet2Meta.setDisplayName(ChatColor.BLUE + "" + ChatColor.BOLD + "FFA " + ChatColor.WHITE + "" + ChatColor.BOLD + "Blade");
			ArrayList<String> lore2 = new ArrayList();
			lore2.add(ChatColor.RED + "Your reliable weapon is always this one.");
			ironhelmet2Meta.setLore(lore2);
			ironhelmet2.setItemMeta(ironhelmet2Meta);

			p.getInventory().addItem(ironhelmet2);
			
			ItemStack ironhelmet = new ItemStack(Material.DIAMOND_AXE);
			ItemMeta ironhelmetMeta = ironhelmet.getItemMeta();
			ironhelmetMeta.setDisplayName(ChatColor.AQUA + "" + ChatColor.ITALIC + "Warriors Battleaxe");
			ArrayList<String> lore1 = new ArrayList();
			lore1.add(ChatColor.RED + "May foes be feared of this cleave.");
			ironhelmetMeta.setLore(lore1);
			ironhelmet.setItemMeta(ironhelmetMeta);
			ironhelmet.addUnsafeEnchantment(Enchantment.DURABILITY, 1000);

			p.getInventory().addItem(ironhelmet);
			
			p.getInventory().addItem(new ItemStack(Material.BOW));
			
			ItemStack ironhelmet3 = new ItemStack(Material.FISHING_ROD);
			ItemMeta ironhelmet3Meta = ironhelmet3.getItemMeta();
			ironhelmet3Meta.setDisplayName(ChatColor.AQUA + "" + ChatColor.ITALIC + "Grappling Hook");
			ArrayList<String> lore3 = new ArrayList();
			lore3.add(ChatColor.GRAY + "May this allow you to evade. Use it well. :)");
			ironhelmet3Meta.setLore(lore3);
			ironhelmet3.setItemMeta(ironhelmet3Meta);
			ironhelmet3.addUnsafeEnchantment(Enchantment.DURABILITY, 1000);

			p.getInventory().addItem(ironhelmet3);
			
			
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.COOKED_BEEF));
			p.getInventory().addItem(new ItemStack(Material.COOKED_BEEF));
			p.getInventory().addItem(new ItemStack(Material.COOKED_BEEF));
			p.getInventory().addItem(new ItemStack(Material.COOKED_BEEF));
			p.getInventory().addItem(new ItemStack(Material.COOKED_BEEF));
			p.getInventory().addItem(new ItemStack(Material.COOKED_BEEF));
			p.getInventory().addItem(new ItemStack(Material.COOKED_BEEF));
			p.getInventory().addItem(new ItemStack(Material.COOKED_BEEF));
			p.getInventory().addItem(new ItemStack(Material.COOKED_BEEF));
			p.getInventory().addItem(new ItemStack(Material.COOKED_BEEF));


		}
		if (command.getName().equalsIgnoreCase("sname")) {
			Player p = (Player) sender;
			p.setDisplayName(args[0]);
		}
		if (command.getName().equalsIgnoreCase("glow")) {
			Player p = (Player) sender;
			if (args[0].equalsIgnoreCase("on")) {
				p.sendMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + "Status" + ChatColor.GRAY + "] Glowing " + ChatColor.GREEN + "ENABLED");
				p.setGlowing(true);
			}
			if (args[0].equalsIgnoreCase("off")) {
				p.sendMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + "Status" + ChatColor.GRAY + "] Glowing " + ChatColor.RED + "DISABLED");
				p.setGlowing(false);
		}
		}
		
		if (command.getName().equalsIgnoreCase("ffa")) {
			Player p = (Player) sender;
			double x = 0.5;
			double y = 101;
			double z = 0.5;

			p.teleport((new Location(Bukkit.getWorld("spawn"), x, y, z)));
			p.setGameMode(GameMode.ADVENTURE);

			p.sendMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + "FFA" + ChatColor.GRAY
					+ "] Welcome to the Free For All Arena!");

			p.getInventory().setHelmet(new ItemStack(Material.IRON_HELMET));
			p.getInventory().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
			p.getInventory().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
			p.getInventory().setBoots(new ItemStack(Material.IRON_BOOTS));
			ItemStack ironhelmet2 = new ItemStack(Material.DIAMOND_SWORD);
			ItemMeta ironhelmet2Meta = ironhelmet2.getItemMeta();
			ironhelmet2Meta.setDisplayName(ChatColor.BLUE + "" + ChatColor.BOLD + "FFA " + ChatColor.WHITE + "" + ChatColor.BOLD + "Blade");
			ArrayList<String> lore2 = new ArrayList();
			lore2.add(ChatColor.RED + "Your reliable weapon is always this one.");
			ironhelmet2Meta.setLore(lore2);
			ironhelmet2.setItemMeta(ironhelmet2Meta);

			p.getInventory().addItem(ironhelmet2);
			
			ItemStack ironhelmet = new ItemStack(Material.DIAMOND_AXE);
			ItemMeta ironhelmetMeta = ironhelmet.getItemMeta();
			ironhelmetMeta.setDisplayName(ChatColor.AQUA + "" + ChatColor.ITALIC + "Warriors Battleaxe");
			ArrayList<String> lore1 = new ArrayList();
			lore1.add(ChatColor.RED + "May foes be feared of this cleave.");
			ironhelmetMeta.setLore(lore1);
			ironhelmet.setItemMeta(ironhelmetMeta);
			ironhelmet.addUnsafeEnchantment(Enchantment.DURABILITY, 1000);

			p.getInventory().addItem(ironhelmet);
			
			p.getInventory().addItem(new ItemStack(Material.BOW));
			
			ItemStack ironhelmet3 = new ItemStack(Material.FISHING_ROD);
			ItemMeta ironhelmet3Meta = ironhelmet3.getItemMeta();
			ironhelmet3Meta.setDisplayName(ChatColor.AQUA + "" + ChatColor.ITALIC + "Grappling Hook");
			ArrayList<String> lore3 = new ArrayList();
			lore3.add(ChatColor.GRAY + "May this allow you to evade. Use it well. :)");
			ironhelmet3Meta.setLore(lore3);
			ironhelmet3.setItemMeta(ironhelmet3Meta);
			ironhelmet3.addUnsafeEnchantment(Enchantment.DURABILITY, 1000);

			p.getInventory().addItem(ironhelmet3);
			
			
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.ARROW));
			p.getInventory().addItem(new ItemStack(Material.COOKED_BEEF));
			p.getInventory().addItem(new ItemStack(Material.COOKED_BEEF));
			p.getInventory().addItem(new ItemStack(Material.COOKED_BEEF));
			p.getInventory().addItem(new ItemStack(Material.COOKED_BEEF));
			p.getInventory().addItem(new ItemStack(Material.COOKED_BEEF));
			p.getInventory().addItem(new ItemStack(Material.COOKED_BEEF));
			p.getInventory().addItem(new ItemStack(Material.COOKED_BEEF));
			p.getInventory().addItem(new ItemStack(Material.COOKED_BEEF));
			p.getInventory().addItem(new ItemStack(Material.COOKED_BEEF));
			p.getInventory().addItem(new ItemStack(Material.COOKED_BEEF));
			


		}

		if (command.getName().equalsIgnoreCase("items")) {

			Player p = (Player) sender;

			if (p.hasPermission("kevin.items")) {

				ItemStack ironhelmet = new ItemStack(Material.TNT);
				ItemMeta ironhelmetMeta = ironhelmet.getItemMeta();
				ironhelmetMeta.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "Throwable TNT");
				ArrayList<String> lore0 = new ArrayList();
				lore0.add(ChatColor.RED + "" + ChatColor.ITALIC + "Boom. Boom.");
				ironhelmetMeta.setLore(lore0);
				ironhelmet.setItemMeta(ironhelmetMeta);
				ironhelmet.addUnsafeEnchantment(Enchantment.DURABILITY, 1000);

				p.getInventory().addItem(ironhelmet);

				ItemStack knightsword = new ItemStack(Material.IRON_SWORD);
				ItemMeta knightswordMeta = knightsword.getItemMeta();
				knightswordMeta.setDisplayName(ChatColor.RED + "Knight's Sword");
				ArrayList<String> lore = new ArrayList();
				lore.add(ChatColor.WHITE + "The knight's sword");
				lore.add(ChatColor.GRAY + "---[" + ChatColor.GOLD + "Defensive Stance" + ChatColor.GRAY + "]---");
				lore.add(ChatColor.WHITE + "Hold down " + ChatColor.AQUA + "Right-Click (Blocking your Sword)");
				lore.add(ChatColor.WHITE + "to activate " + ChatColor.GOLD + "Defensive Mode" + ChatColor.WHITE + "!");
				lore.add(ChatColor.WHITE + "This will give you Damage Resistance and Regen!!");
				lore.add(ChatColor.RED + "Releasing block will no longer give you Damage Resistance and Regen!");
				lore.add(ChatColor.GRAY + "---[" + ChatColor.YELLOW + "Absorb" + ChatColor.GRAY + "]---");
				lore.add(ChatColor.AQUA + "Shift " + ChatColor.WHITE + "and " + ChatColor.AQUA
						+ "Right-Click (Blocking your Sword)");
				lore.add(ChatColor.WHITE + "to activate " + ChatColor.YELLOW + "Absorb" + ChatColor.WHITE + "!");
				lore.add(ChatColor.WHITE + "This will give you Absorbation!");
				knightswordMeta.setLore(lore);
				knightsword.setItemMeta(knightswordMeta);
				knightsword.addEnchantment(Enchantment.DAMAGE_ALL, 1);
				knightsword.addUnsafeEnchantment(Enchantment.DURABILITY, 1000);

				p.getInventory().addItem(new ItemStack[] { knightsword });

				ItemStack woodensword = new ItemStack(Material.WOOD_SWORD);
				ItemMeta woodenswordMeta = woodensword.getItemMeta();
				woodenswordMeta.setDisplayName(ChatColor.WHITE + "Archer's Dagger");
				ArrayList<String> lore1 = new ArrayList();
				lore1.add(ChatColor.GRAY + "Your only little dagger");
				lore1.add(ChatColor.GRAY + "---[" + ChatColor.RED + "Hunter's" + ChatColor.DARK_PURPLE + "Fury"
						+ ChatColor.GRAY + "]---");
				lore1.add(ChatColor.AQUA + "Right CLick " + ChatColor.WHITE + "to use");
				lore1.add(ChatColor.YELLOW + "Hunter's Fury");
				lore1.add(ChatColor.LIGHT_PURPLE + "You will gain alot of buffs! Really!");
				woodenswordMeta.setLore(lore1);
				woodensword.setItemMeta(woodenswordMeta);
				woodensword.addEnchantment(Enchantment.KNOCKBACK, 1);
				woodensword.addUnsafeEnchantment(Enchantment.DURABILITY, 1000);
				woodensword.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 1);

				p.getInventory().addItem(new ItemStack[] { woodensword });

				ItemStack magesword = new ItemStack(Material.DIAMOND_HOE);
				/* 652 */ ItemMeta mageswordMeta = magesword.getItemMeta();
				/* 653 */ mageswordMeta.setDisplayName(ChatColor.RED + " " + ChatColor.BOLD + "Gnar's Claw");
				/* 654 */ ArrayList<String> lore2 = new ArrayList();
				/* 655 */ lore2.add(ChatColor.RED + "Be Gnar! Pull in with your claws!");
				/* 656 */ lore2.add(ChatColor.GRAY + "---[" + ChatColor.RED + "GNAR!" + ChatColor.GRAY +
				/* 657 */ "]---");
				/* 658 */ lore2.add(ChatColor.AQUA + "Right Click " + ChatColor.WHITE + "to activate " +
				/* 659 */ ChatColor.RED + "GNAR!");
				/* 660 */ lore2.add(ChatColor.YELLOW +
				/* 661 */ "Pull your enemies in a targeted direction and deal serious damage!");
				/* 662 */ mageswordMeta.setLore(lore2);
				/* 663 */ magesword.setItemMeta(mageswordMeta);
				/* 664 */ magesword.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 5);

				p.getInventory().addItem(new ItemStack[] { magesword });

			}
		}

		return false;
	}

}
