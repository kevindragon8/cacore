package me.kevindragon.cacore;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class Main extends JavaPlugin implements Listener {

	private HashMap<Player, Integer> cooldownTime;
	private HashMap<Player, BukkitRunnable> cooldownTask;

	public static Plugin pl;
	public Permission items = new Permission("kevin.items");
	public Permission announce = new Permission("kevin.announce");
	public Permission beta = new Permission("kevin.beta");

	public void onEnable() {

		this.cooldownTime = new HashMap();
		this.cooldownTask = new HashMap();

		PluginManager pm = Bukkit.getServer().getPluginManager();
		pm.registerEvents(new events(), this);
		pm.registerEvents(new elytrawars(), this);
		pm.registerEvents(new arena(), this);
		getCommand("arena").setExecutor(new command());
		getCommand("build").setExecutor(new command());
		getCommand("spawn").setExecutor(new command());
		getCommand("worldedittool").setExecutor(new command());
		getCommand("coreinfo").setExecutor(new command());
		getCommand("items").setExecutor(new command());
		getCommand("news").setExecutor(new command());
		getCommand("sannounce").setExecutor(new command());
		getCommand("poke").setExecutor(new command());
		getCommand("ffa2").setExecutor(new command());
		getCommand("ffa").setExecutor(new command());
		getCommand("staff").setExecutor(new command());
		getCommand("ts").setExecutor(new command());
		getCommand("docs").setExecutor(new command());
		getCommand("warn").setExecutor(new command());
		getCommand("donate").setExecutor(new command());
		getCommand("spotion").setExecutor(new command());
		getCommand("switchblade").setExecutor(new command());
		getCommand("apply").setExecutor(new command());
		getCommand("sname").setExecutor(new command());
		getCommand("update").setExecutor(new command());
		getCommand("commands").setExecutor(new command());
		getCommand("event").setExecutor(new command());
		getCommand("glow").setExecutor(new command());
		getCommand("arena").setExecutor(new arena());
		getCommand("goldensword").setExecutor(new command());
		
		getCommand("ew").setExecutor(new elytrawars());
		Bukkit.broadcastMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + "Plugin" + ChatColor.GRAY + "] " + ChatColor.AQUA + "Core Plugin " + ChatColor.GRAY + "has been " + ChatColor.GREEN + "Enabled! Spigot 1.9!");

	}

	public void onDiable() {
		Bukkit.broadcastMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + "Plugin" + ChatColor.GRAY + "] " + ChatColor.AQUA + "Core Plugin " + ChatColor.GRAY + "has been " + ChatColor.RED + "Disabled!");
	}

}
