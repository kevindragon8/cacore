package me.kevindragon.cacore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import com.connorlinfoot.bountifulapi.BountifulAPI;

public class events implements Listener {

	public static final ArrayList<Object> cooldowns = new ArrayList();
	public static final ArrayList<Object> launch = new ArrayList();

	HashMap<UUID, PermissionAttachment> perms = new HashMap<UUID, PermissionAttachment>();

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {

		Player p = (Player) e.getPlayer();

		/*
		 * double x = 0.5; double y = 65; double z = 0.5;
		 * 
		 * p.teleport((new Location(Bukkit.getWorld("plots"), x, y, z)));
		 * p.setGameMode(GameMode.CREATIVE);
		 * 
		 * 
		 * 
		 * p.sendMessage(""); p.sendMessage("              " + ChatColor.GRAY +
		 * "" + ChatColor.BOLD + "" + ChatColor.STRIKETHROUGH + "--[" +
		 * ChatColor.AQUA + "" + ChatColor.BOLD + "C.A." + ChatColor.GREEN + ""
		 * + ChatColor.BOLD + "BUILD" + ChatColor.GRAY + "" + ChatColor.BOLD +
		 * "" + ChatColor.STRIKETHROUGH + "]--"); p.sendMessage(ChatColor.AQUA +
		 * "" + ChatColor.BOLD + "           Welcome! :)"); p.sendMessage("");
		 * 
		 */

		
		p.sendMessage("                   " + ChatColor.GOLD + "" + ChatColor.BOLD + ">> " + ChatColor.AQUA + "" + ChatColor.BOLD + "C.A. " + ChatColor.BLUE + "" + ChatColor.BOLD + "STAFF " + ChatColor.WHITE + "" + ChatColor.BOLD + "IS OPEN!!");
		p.sendMessage(ChatColor.YELLOW + "    Applications are now open! Do /apply!");
		p.sendMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + "Staff" + ChatColor.GRAY + "] " + ChatColor.GRAY + "Moderator Applications are open! /apply!");
	}

	/*
	 * public void addToCooldown(final Player p, int t) { cooldowns.add(p);
	 * Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.pl, new
	 * Runnable() { public void run() { events.cooldowns.remove(p);
	 * p.sendMessage(ChatColor.AQUA + " " + ChatColor.BOLD + ">> " +
	 * ChatColor.GREEN + "Your Ability is ready for use!");
	 * p.playSound(p.getLocation(), Sound.ORB_PICKUP, 0.0F, 2.0F); }
	 * 
	 * }, t); } // bunnyhop game? different kits = different bunny hop speeds +
	 * parkour?
	 * 
	 * if (p.isOnGround()) {
	 * 
	 * if (p.isSneaking()) {
	 * 
	 * ItemStack item = new ItemStack(Material.FEATHER); ItemMeta itemMeta =
	 * item.getItemMeta(); itemMeta.setDisplayName(ChatColor.LIGHT_PURPLE +
	 * "B-Hopping Stacks"); ArrayList<String> lore1 = new ArrayList();
	 * lore1.add(ChatColor.RED + "This counts your stacks of B-Hopping!");
	 * itemMeta.setLore(lore1); item.setItemMeta(itemMeta);
	 * item.addUnsafeEnchantment(Enchantment.DURABILITY, 1000);
	 * 
	 * p.getInventory().addItem(item);
	 * 
	 * // FIX // double x = 0.1D.;
	 * 
	 * Vector leap = p.getLocation().getDirection().multiply(1.0D).setY(0.5D);
	 * 
	 * p.setVelocity(leap);
	 * 
	 * } }
	 * 
	 * }
	 */

	@EventHandler
	public void onEntityDmg(EntityDamageByEntityEvent e) {
			Player p = (Player) e.getEntity();
			Player p1 = (Player) e.getDamager();
					
			double dmg = e.getDamage();
			p.sendMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + "Damage" + ChatColor.GRAY + "] You took " + ChatColor.RED + dmg + ChatColor.GRAY + " damage from " + ChatColor.BLUE + p1.getDisplayName());
			p1.sendMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + "Damage" + ChatColor.GRAY + "] You gave " + ChatColor.RED + dmg + ChatColor.GRAY + " damage to " + ChatColor.BLUE + p.getDisplayName());
	}
	
	@EventHandler

	// Everything Death
	public void onDeath(EntityDeathEvent e) {
		ItemStack item = new ItemStack(Material.GOLD_NUGGET);
		ItemMeta itemMeta = item.getItemMeta();
		itemMeta.setDisplayName(ChatColor.GOLD + "Gold Bit");
		ArrayList<String> lore1 = new ArrayList();
		lore1.add(ChatColor.RED + "This is the lowest tier of Gold Currency!");
		itemMeta.setLore(lore1);
		item.setItemMeta(itemMeta);

		ItemStack item1 = new ItemStack(Material.GOLD_INGOT);
		ItemMeta item1Meta = item1.getItemMeta();
		item1Meta.setDisplayName(ChatColor.GOLD + "Gold Loaf");
		ArrayList<String> lore2 = new ArrayList();
		lore2.add(ChatColor.RED + "This is the second tier of Gold Currency!");
		item1Meta.setLore(lore2);
		item1.setItemMeta(item1Meta);

		ItemStack item2 = new ItemStack(Material.GOLD_BLOCK);
		ItemMeta item2Meta = item2.getItemMeta();
		item2Meta.setDisplayName(ChatColor.GOLD + "Gold Slab");
		ArrayList<String> lore3 = new ArrayList();
		lore3.add(ChatColor.RED + "This is the third tier of Gold Currency!");
		item2Meta.setLore(lore3);
		item2.setItemMeta(item2Meta);

		e.getDrops().add(item1);

		// Enderman Death
		if (e.getEntityType().equals(EntityType.ENDERMAN)) {

			e.getDrops().add(item2);
			e.getDrops().remove(item);

		}
		// Witch Death
		if (e.getEntityType().equals(EntityType.WITCH)) {

			e.getDrops().add(item2);
			e.getDrops().remove(item);

		}

		if (e.getEntityType().equals(EntityType.PLAYER)) {

			e.getDrops().add(item2);
			e.getDrops().remove(item1);

		}

	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		Player p = (Player) e.getPlayer();
		
		if (e.getAction().equals(Action.RIGHT_CLICK_AIR)) {
			ItemStack item2 = new ItemStack(Material.GOLD_BLOCK);
			ItemMeta item2Meta = item2.getItemMeta();
			item2Meta.setDisplayName(ChatColor.GOLD + "Gold Slab");
			ArrayList<String> lore3 = new ArrayList();
			lore3.add(ChatColor.RED + "This is the third tier of Gold Currency!");
			item2Meta.setLore(lore3);
			item2.setItemMeta(item2Meta);
			
			ItemStack ironhelmet2 = new ItemStack(Material.GOLD_SWORD);
			ItemMeta ironhelmet2Meta = ironhelmet2.getItemMeta();
			ironhelmet2Meta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "GOLDEN " + ChatColor.YELLOW + "" + ChatColor.BOLD + "BLADE");
			ArrayList<String> lore2 = new ArrayList();
			lore2.add(ChatColor.AQUA + "" + ChatColor.ITALIC + "Legendary" + ChatColor.GRAY + "-" + ChatColor.RED + "May this weapon use the power of GOLD!");
			ironhelmet2Meta.setLore(lore2);
			ironhelmet2.setItemMeta(ironhelmet2Meta);
			
			if (p.getItemInHand().equals(ironhelmet2))  {
				
				 for (Entity entities : p.getNearbyEntities(5.0D, 5.0D, 5.0D)) {
			if ((entities instanceof LivingEntity)) {
			LivingEntity entity = (LivingEntity)entities;
			int dmg = item2.getAmount()*4;
			
			p.sendMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + "Ability" + ChatColor.GRAY + "] You dealt " + ChatColor.RED + dmg + ChatColor.GRAY + " damage to nearby players!");
			entity.damage(dmg);
			p.getInventory().remove(item2);
			entity.sendMessage(ChatColor.GRAY + "[" + ChatColor.RED + "Damage" + ChatColor.GRAY + "] You have taken " + ChatColor.BLUE + dmg + ChatColor.GRAY + " damage from " + ChatColor.BLUE + p.getDisplayName());
			
			}
				 }
				
			}
		}
		
		if (e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			if (p.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.BLUE + "" + ChatColor.ITALIC
					+ "Ambiguous " + ChatColor.WHITE + "Potion " + ChatColor.AQUA + "" + ChatColor.BOLD + "I")) {
				PotionEffect slow = PotionEffectType.SLOW.createEffect(200, 2);
				p.addPotionEffect(slow);

				PotionEffect regen = PotionEffectType.REGENERATION.createEffect(200, 2);
				p.addPotionEffect(regen);
				p.sendMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + "Status" + ChatColor.GRAY + "] You drank "
						+ ChatColor.BLUE + "" + ChatColor.ITALIC + "Ambiguous " + ChatColor.WHITE + "Potion "
						+ ChatColor.AQUA + "" + ChatColor.BOLD + "I");
				p.setItemInHand(new ItemStack(Material.GLASS_BOTTLE));
			}
		}

		if (e.getAction().equals(Action.LEFT_CLICK_AIR) || e.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
			if (p.isSneaking()) {
				if (p.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.BLUE + "" + ChatColor.BOLD
						+ "SWITCH " + ChatColor.WHITE + "" + ChatColor.BOLD + "Blade")) {

					ItemStack ironhelmet = new ItemStack(Material.IRON_SWORD);
					ItemMeta ironhelmetMeta = ironhelmet.getItemMeta();
					ironhelmetMeta.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "SWITCH " + ChatColor.WHITE + ""
							+ ChatColor.BOLD + "Blade");
					ArrayList<String> lore1 = new ArrayList();
					lore1.add(ChatColor.BLUE + "" + ChatColor.ITALIC + "Legendary" + ChatColor.GRAY + "-" +ChatColor.RED + "Be powerful, but weakened at once.");
					ironhelmetMeta.setLore(lore1);
					ironhelmet.setItemMeta(ironhelmetMeta);

					p.setItemInHand(ironhelmet);
				}
			}
		}

		if (e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			if (p.isSneaking()) {
				if (p.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RED + "" + ChatColor.BOLD
						+ "SWITCH " + ChatColor.WHITE + "" + ChatColor.BOLD + "Blade")) {

					ItemStack ironhelmet = new ItemStack(Material.DIAMOND_SWORD);
					ItemMeta ironhelmetMeta = ironhelmet.getItemMeta();
					ironhelmetMeta.setDisplayName(ChatColor.BLUE + "" + ChatColor.BOLD + "SWITCH " + ChatColor.WHITE
							+ "" + ChatColor.BOLD + "Blade");
					ArrayList<String> lore1 = new ArrayList();
					lore1.add(ChatColor.BLUE + "" + ChatColor.ITALIC + "Legendary" + ChatColor.GRAY + "-" +ChatColor.RED + "Be powerful, but weakened at once.");
					ironhelmetMeta.setLore(lore1);
					ironhelmet.setItemMeta(ironhelmetMeta);
					ironhelmet.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 7);

					p.setItemInHand(ironhelmet);
				}
			}
		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e) {
		Player p = (Player) e.getEntity();
		Player p1 = (Player) e.getDamager();
		ItemStack ironhelmet2 = new ItemStack(Material.GOLD_SWORD);
		ItemMeta ironhelmet2Meta = ironhelmet2.getItemMeta();
		ironhelmet2Meta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "GOLDEN " + ChatColor.YELLOW + "" + ChatColor.BOLD + "BLADE");
		ArrayList<String> lore2 = new ArrayList();
		lore2.add(ChatColor.AQUA + "" + ChatColor.ITALIC + "Legendary" + ChatColor.GRAY + "-" + ChatColor.RED + "Your reliable weapon is always this one.");
		ironhelmet2Meta.setLore(lore2);
		ironhelmet2.setItemMeta(ironhelmet2Meta);
		
		ItemStack item2 = new ItemStack(Material.GOLD_BLOCK);
		ItemMeta item2Meta = item2.getItemMeta();
		item2Meta.setDisplayName(ChatColor.GOLD + "Gold Slab");
		ArrayList<String> lore3 = new ArrayList();
		lore3.add(ChatColor.RED + "This is the third tier of Gold Currency!");
		item2Meta.setLore(lore3);
		item2.setItemMeta(item2Meta);
		if (p1.getItemInHand().equals(ironhelmet2)) {
			p1.getInventory().addItem(item2);
			
		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onHeld(PlayerMoveEvent e) {
		Player p = (Player) e.getPlayer();
		if (p.getItemInHand().getItemMeta().getDisplayName().equals(
				ChatColor.BLUE + "" + ChatColor.BOLD + "SWITCH " + ChatColor.WHITE + "" + ChatColor.BOLD + "Blade")) {
			PotionEffect poison = PotionEffectType.POISON.createEffect(100, 1);

			p.addPotionEffect(poison);

			PotionEffect speed = PotionEffectType.SPEED.createEffect(100, 3);

			p.addPotionEffect(speed);
			p.damage(0);
		}

		if (p.getItemInHand().getItemMeta().getDisplayName().equals(
				ChatColor.RED + "" + ChatColor.BOLD + "SWITCH " + ChatColor.WHITE + "" + ChatColor.BOLD + "Blade")) {
			PotionEffect poison = PotionEffectType.REGENERATION.createEffect(100, 1);

			p.addPotionEffect(poison);
		}
	}

	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e) {

		Player p = (Player) e.getEntity();


		if (p.getWorld().equals("spawn")) {
			double x = 0.5;
			double y = 101;
			double z = 0.5;
			p.teleport(new Location(Bukkit.getWorld("spawn"), x, y, z));
		} else {
			double x = -268.5;
			double y = 71;
			double z = 284.5;
			p.teleport(new Location(Bukkit.getWorld("world"), x, y, z));
		}
		e.setDeathMessage(ChatColor.GRAY + "[" + ChatColor.BLUE + "Death" + ChatColor.GRAY + "] " + ChatColor.BLUE + p.getDisplayName()  + ChatColor.GRAY + " has been killed by " + ChatColor.BLUE + p.getKiller().getDisplayName());
		
		

		/*
		 * 
		 * ItemStack item = new ItemStack(Material.GOLD_NUGGET); ItemMeta
		 * itemMeta = item.getItemMeta(); itemMeta.setDisplayName(ChatColor.GOLD
		 * + "Gold Shard"); ArrayList<String> lore1 = new ArrayList();
		 * lore1.add(ChatColor.RED + "This is the lowest tier of Gold Currency!"
		 * ); itemMeta.setLore(lore1); item.setItemMeta(itemMeta);
		 * 
		 * e.getDrops().add(item); e.getDrops().add(item);
		 * e.getDrops().add(item); e.getDrops().add(item);
		 * e.getDrops().add(item);
		 * 
		 */

		p.setGameMode(GameMode.SURVIVAL);
		e.setDroppedExp(200);

		if (p.getLastDamageCause().equals(DamageCause.BLOCK_EXPLOSION)) {
			e.setDeathMessage(ChatColor.GRAY + "[" + ChatColor.RED + "Death" + ChatColor.GRAY + "] " + ChatColor.RED
					+ p.getDisplayName() + ChatColor.WHITE + " was blown up into pieces!");
		}
		if (p.getLastDamageCause().equals(DamageCause.DROWNING)) {
			e.setDeathMessage(ChatColor.GRAY + "[" + ChatColor.RED + "Death" + ChatColor.GRAY + "] " + ChatColor.RED
					+ p.getDisplayName() + ChatColor.WHITE + " forgot how to breathe.");
		}
		if (p.getLastDamageCause().equals(DamageCause.ENTITY_ATTACK)) {
			e.setDeathMessage(ChatColor.GRAY + "[" + ChatColor.RED + "Death" + ChatColor.GRAY + "] " + ChatColor.RED
					+ p.getDisplayName() + ChatColor.WHITE + " died by a " + ChatColor.RED + p.getLastDamage());
		}
		if (p.getLastDamageCause().equals(DamageCause.ENTITY_EXPLOSION)) {
			e.setDeathMessage(ChatColor.GRAY + "[" + ChatColor.RED + "Death" + ChatColor.GRAY + "] " + ChatColor.RED
					+ p.getDisplayName() + ChatColor.WHITE + " forgot that Creepers explode.");
		}
		if (p.getLastDamageCause().equals(DamageCause.FALL)) {
			e.setDeathMessage(ChatColor.GRAY + "[" + ChatColor.RED + "Death" + ChatColor.GRAY + "] " + ChatColor.RED
					+ p.getDisplayName() + ChatColor.WHITE + " thought Fall Damage was disabled. ");
		}
		if (p.getLastDamageCause().equals(DamageCause.FALLING_BLOCK)) {
			e.setDeathMessage(ChatColor.GRAY + "[" + ChatColor.RED + "Death" + ChatColor.GRAY + "] " + ChatColor.RED
					+ p.getDisplayName() + ChatColor.WHITE + " was buried in blocks. ");
		}
		if (p.getLastDamageCause().equals(DamageCause.FIRE) || p.getLastDamageCause().equals(DamageCause.FIRE_TICK)) {
			e.setDeathMessage(ChatColor.GRAY + "[" + ChatColor.RED + "Death" + ChatColor.GRAY + "] " + ChatColor.RED
					+ p.getDisplayName() + ChatColor.WHITE + " didn't realize they were on fire. Literally.");
		}
		if (p.getLastDamageCause().equals(DamageCause.LAVA)) {
			e.setDeathMessage(ChatColor.GRAY + "[" + ChatColor.RED + "Death" + ChatColor.GRAY + "] " + ChatColor.RED
					+ p.getDisplayName() + ChatColor.WHITE + " thought Lava was Water. #Trolled");
		}
		if (p.getLastDamageCause().equals(DamageCause.LIGHTNING)) {
			e.setDeathMessage(ChatColor.GRAY + "[" + ChatColor.RED + "Death" + ChatColor.GRAY + "] " + ChatColor.RED
					+ p.getDisplayName() + ChatColor.WHITE + " was Electrocuted. Ouch.");
		}
		if (p.getLastDamageCause().equals(DamageCause.MAGIC)) {
			e.setDeathMessage(ChatColor.GRAY + "[" + ChatColor.RED + "Death" + ChatColor.GRAY + "] " + ChatColor.RED
					+ p.getDisplayName() + ChatColor.WHITE + " couldn't find cures.");
		}
		if (p.getLastDamageCause().equals(DamageCause.PROJECTILE)) {
			e.setDeathMessage(ChatColor.GRAY + "[" + ChatColor.RED + "Death" + ChatColor.GRAY + "] " + ChatColor.RED
					+ p.getDisplayName() + ChatColor.WHITE + " was shot down by " + ChatColor.RED + p.getLastDamage());
		}
		if (p.getLastDamageCause().equals(DamageCause.STARVATION)) {
			e.setDeathMessage(ChatColor.GRAY + "[" + ChatColor.RED + "Death" + ChatColor.GRAY + "] " + ChatColor.RED
					+ p.getDisplayName() + ChatColor.WHITE + " forgot to Eat their Vegetables.");
		}
		if (p.getLastDamageCause().equals(DamageCause.SUFFOCATION)) {
			e.setDeathMessage(ChatColor.GRAY + "[" + ChatColor.RED + "Death" + ChatColor.GRAY + "] " + ChatColor.RED
					+ p.getDisplayName() + ChatColor.WHITE + " didn't realize blocks hurt.");
		}
		if (p.getLastDamageCause().equals(DamageCause.SUICIDE)) {
			e.setDeathMessage(ChatColor.GRAY + "[" + ChatColor.RED + "Death" + ChatColor.GRAY + "] " + ChatColor.RED
					+ p.getDisplayName() + ChatColor.WHITE + " has ended their life. D:");
		}
		if (p.getLastDamageCause().equals(DamageCause.VOID)) {
			e.setDeathMessage(ChatColor.GRAY + "[" + ChatColor.RED + "Death" + ChatColor.GRAY + "] " + ChatColor.RED
					+ p.getDisplayName() + ChatColor.WHITE + " somehow broke the server by falling into the void.");
		}
	}

	@EventHandler
	public void onBreak(BlockBreakEvent e) {

		Player p = (Player) e.getPlayer();

		ItemStack item1 = new ItemStack(Material.GOLD_INGOT);
		ItemMeta item1Meta = item1.getItemMeta();
		item1Meta.setDisplayName(ChatColor.GOLD + "Gold Loaf");
		ArrayList<String> lore2 = new ArrayList();
		lore2.add(ChatColor.RED + "This is the second tier of Gold Currency!");
		item1Meta.setLore(lore2);
		item1.setItemMeta(item1Meta);

		if (e.getBlock().equals(Material.GOLD_ORE)) {
			e.getBlock().getDrops().clear();
			e.getBlock().getDrops().add(item1);

			BountifulAPI.sendActionBar(p, ChatColor.YELLOW + "You mined a " + ChatColor.GOLD + "Gold Ore!");
		}
	}

	@EventHandler
	public void onPickupEvent(PlayerPickupItemEvent e) {

		Player p = (Player) e.getPlayer();

		ItemStack item = new ItemStack(Material.GOLD_NUGGET);
		ItemMeta itemMeta = item.getItemMeta();
		itemMeta.setDisplayName(ChatColor.GOLD + "Gold Shard");
		ArrayList<String> lore1 = new ArrayList();
		lore1.add(ChatColor.RED + "This is the lowest tier of Gold Currency!");
		itemMeta.setLore(lore1);
		item.setItemMeta(itemMeta);

		if (e.getItem().equals(item)) {

			p.sendMessage(ChatColor.GRAY + "" + ChatColor.BOLD + ">> " + ChatColor.GRAY + "You picked up "
					+ ChatColor.GOLD + "Gold!");

			// exp gain p.playSound(p.getLocation(), Sound., arg2, arg3);

		}
	}

	@EventHandler
	public void onCraft(PrepareItemCraftEvent e) {
		if (e.getInventory().getResult().equals(Material.ENCHANTMENT_TABLE)) {
			e.getInventory().setResult(null);
		}

	}

}
